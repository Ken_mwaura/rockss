# Rockss

Rockss aims to help web-designers to kickstarts their projects faster by defining a rich set of reusable element styles which are fully customizeable trough Sass variables. It is built on top of **bourbon** and **neat**, *rockss* also needs a css-reset file like Normalize.css


**Requirements:**
- [Bourbon](http://bourbon.io)
- [Neat](http://neat.bourbon.io)
- [Normalize](http://necolas.github.io/normalize.css/)


## Example page

See rockss default styles in action on the [Rockss.examplepage](https://thomasfuston.gitlab.io/rockss/)


## Installation

There are 2 options to using Rockss;


**First - plain rockss.css/rockss.min.css file**
(already includes bourbon/neat/normalize)

1. Cloning **Rockss from gitlab repository
2. Link **css/rockss.min.css** in your template file.
3. Enjoy **Rockss** as foundation.


**Second - sass files sass/rocks/** 

1. Clone **Rockss** from gitlab.
2. Import Rockss in your project after Bourbon and Neat and Normalize are imported
3. Open *_grid-settings.scss* and change the import path for *neat/neat-helpers*.
4. Open *_variables.scss* and start to modify everything to your needs.


## License

Rockss is free to use and abuse under the 
[open-source MIT license](https://gitlab.com/ThomasFuston/rockss/blob/master/LICENSE) 
created by [Thomas Fuston](https://gitlab.com/u/ThomasFuston).
